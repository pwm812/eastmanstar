using Microsoft.VisualStudio.TestTools.UnitTesting;
using Consul;

namespace UnitTestProject1
{
	[TestClass]
	public class ConsulTest
	{
		const string _consulAddress = "http://MyWinServer1:8500";

		[TestMethod]
		public void GetAllServices()
		{
			var client = new ConsulClient(cfg =>
			{
				cfg.Address = new System.Uri(_consulAddress);
			});
			var services = client.Agent.Services().Result;
		}
	}
}
