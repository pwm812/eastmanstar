﻿using MicroService.BaseService.Controllers;
using MicroService.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

using System.Linq;

namespace TestService1.Controllers
{
    /// <summary>
    /// Test service home controller
    /// </summary>
    public class HomeController : BaseAPIController
    {
        /// <summary>
        /// haha
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Haha()
        {
            var result = JsonConvert.SerializeObject(HttpContext.User.Claims.Select(a => new { a.Type, a.Value }));

            return Ok($"TestService1：{result}");
        }

        /// <summary>
        /// need authentication
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult NeedsAuthentication()
        {
            return Ok($"NeedsAuthentication：{HttpContext.Request.Host.Port}");
        }

        /// <summary>
        /// Allow anonymous
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Descryt(UserInput user)
        {
            return Ok(MessageResult.SuccessResult(user));
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Descryt1([FromBody] string user)
        {
            return Ok(MessageResult.SuccessResult(user));
        }
    }

    public class UserInput
    {
        public string Account { get; set; }

        public string Password { get; set; }
    }
}
