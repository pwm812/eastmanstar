﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace TestService1
{
    public class Startup : MicroService.BaseService.BaseStartup
    {
        IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public override void AfterConfigure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //do somethings
            app.ApplicationServices.GetService<ILogger<Startup>>().LogInformation(_configuration["ConsulRegisterSettings:Address"]);
            app.ApplicationServices.GetService<ILogger<Startup>>().LogInformation(_configuration["ConsulRegisterSettings:ServiceSettings:IP"]);

        }

        public override void AfterConfigureServices(IServiceCollection services)
        {
            //add bearer auth
            services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
            {
                var section = _configuration.GetSection("IdentityServerSettings");
                options.Authority = section["Authority"];//IdentityServer address
                options.RequireHttpsMetadata = false;
                options.Audience = section["Audience"];
            });
        }
    }
}
