﻿using MicroService.BaseService.Controllers;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.Wechat.Controllers
{
	public class HomeController : BaseAPIController
	{
		[HttpGet]
		public IActionResult Haha()
		{
			var result = JsonConvert.SerializeObject(HttpContext.User.Claims.Select(a => new { a.Type, a.Value }));

			return Ok($"Wechat：{result}");
		}
	}
}
