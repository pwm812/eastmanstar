﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    ///Add and modify business model of role table
    /// </summary>
    public class RoleInput
    {
        /// <summary>
        /// Role Name
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// Role Name
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Parent Role Id
        /// Dealing with hierarchical relationships between roles
        /// </summary>
        public long? ParentRoleId { get; set; }

        /// <summary>
        /// Description About Role
        /// </summary>
        public string Description { get; set; }
    }
}
