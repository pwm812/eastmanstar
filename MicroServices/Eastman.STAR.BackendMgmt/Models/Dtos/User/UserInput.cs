﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
	/// <summary>
	/// Edit user input obj
	/// </summary>
	public class UserInput
	{
		public long? Id { get; set; }

		public long OrganizationId { get; set; }

		public string Name { get; set; }

		public string Account { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }
	}
}
