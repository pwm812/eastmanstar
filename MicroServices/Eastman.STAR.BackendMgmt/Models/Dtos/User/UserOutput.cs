﻿using MicroService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
	public class UserOutput
	{
		public string Name { get; set; }

		public string Account { get; set; }

		public string avatar { get; set; }

        public string[] roles { get; set; }

        public string introduction { get; set; }
    }

	public class UserPageOutput
	{
		public int total { get; set; }

		public List<User> data { get; set; } = new List<User>();
	}
}
