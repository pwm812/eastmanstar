﻿using MicroService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Models.Dtos
{
    /// <summary>
    /// Return Busienss Model
    /// </summary>
    public class ReturnMessage
    {
        /// <summary>
        /// Interface return value status
        /// </summary>
        public bool Status { get; set; } = true;

        /// <summary>
        /// Return Message Business Model
        /// </summary>
        public MessageModel Message { get; set; }
    }
}
