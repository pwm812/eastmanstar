﻿using AutoMapper;
using Eastman.STAR.BackendMgmt.Models.Dtos;
using MicroService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
    /// <summary>
    /// Role Service
    /// </summary>
    public class RoleServices
    {
        readonly IRepository<Role> _repositoryRole;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="repositoryRole"></param>
        /// <param name="mapper"></param>
        public RoleServices(IRepository<Role> repositoryRole, IMapper mapper)
        {
            _repositoryRole = repositoryRole;
            _mapper = mapper;
        }

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="name"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<List<Role>> GetRoles(string name, PaginationRequest request)
        {
            var result = _repositoryRole.Entities.ToList();
            if (!string.IsNullOrEmpty(name))
            {
                result = result.Where(s => s.RoleName.Contains(name)).ToList();
            }

            result = result.Skip(request.PageIndex * request.PageSize).Take(request.PageSize).ToList();
            request.TotalCount = result.Count;
            return Task.FromResult(result);
        }

        /// <summary>
        /// Get Single Role By Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public Task<Role> GetRole(long roleId)
        {
            var result = _repositoryRole.Entities.SingleOrDefaultAsync(s => s.Id == roleId && !s.IsDeleted);
            return result;
        }

        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<ReturnMessage> AddRole(RoleInput request, long currentLoginUserId)
        {
            if (request.Id != null && request.Id.Value > 0)
            {
                return new Task<ReturnMessage>(() => new ReturnMessage { Status = false, Message = MessageModel.InvalidIdMessage });
            }

            var isExistRoleName = _repositoryRole.Entities.FirstOrDefault(s => s.RoleName == request.RoleName && !s.IsDeleted);
            if (isExistRoleName != null)
            {
                return Task.FromResult(new ReturnMessage { Status = false, Message = MessageModel.AddOrUpdateToBeAnExistedRoleName });
            }
            var add_role_model = new Role();
            add_role_model = _mapper.Map<Role>(request);
            add_role_model.CreatedAt = DateTime.Now;
            add_role_model.CreatedBy = currentLoginUserId;

            _repositoryRole.Add(add_role_model);
            return Task.FromResult(new ReturnMessage { Status = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<ReturnMessage> UpdateRole(RoleInput request, long currentLoginUserId)
        {
            if (request.Id == null || request.Id.Value <= 0)
            {
                return new Task<ReturnMessage>(() => new ReturnMessage { Status = false, Message = MessageModel.InvalidIdMessage });
            }

            var isExistRoleName = _repositoryRole.Entities.FirstOrDefault(s => s.Id != request.Id && s.RoleName == request.RoleName && !s.IsDeleted);
            if (isExistRoleName != null)
            {
                return Task.FromResult(new ReturnMessage { Status = false, Message = MessageModel.AddOrUpdateToBeAnExistedRoleName });
            }

            var update_role_model = _repositoryRole.Entities.FirstOrDefault(s => s.Id == request.Id && !s.IsDeleted);
            if (update_role_model == null)
            {
                return Task.FromResult(new ReturnMessage { Status = false, Message = MessageModel.IdOrAccountChangedMessage });
            }

            _mapper.Map(request, update_role_model);
            update_role_model.ModifiedAt = DateTime.Now;
            update_role_model.ModifiedBy = currentLoginUserId;

            _repositoryRole.Modify(update_role_model);
            return Task.FromResult(new ReturnMessage { Status = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="currentLoginUserId"></param>
        /// <returns></returns>
        public Task<ReturnMessage> DeleteRole(long roleId, long currentLoginUserId)
        {
            var delete_role_model = _repositoryRole.Entities.FirstOrDefault(s => s.Id == roleId && !s.IsDeleted);
            delete_role_model.IsDeleted = true;
            delete_role_model.DeletedAt = DateTime.Now;
            delete_role_model.DeletedBy = currentLoginUserId;

            _repositoryRole.Modify(delete_role_model);
            return Task.FromResult(new ReturnMessage { Status = _repositoryRole.SaveChangesAsync().Result > 0 });
        }

        /// <summary>
        /// Check Role Name Is Empty
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<ReturnMessage> CheckRole(RoleInput request)
        {
            return string.IsNullOrEmpty(request.RoleName)
                ? Task.FromResult(new ReturnMessage { Status = false, Message = MessageModel.RoleNameIsEmptyMessage })
                : Task.FromResult(new ReturnMessage());
        }
    }
}
