﻿using Eastman.STAR.BackendMgmt.Models.Dtos;

using MicroService.Models;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Services
{
	/// <summary>
	/// User services class
	/// </summary>
	public class UserServices
	{
		IRepository<User> _repositoryUser;
		public UserServices(IRepository<User> repositoryUser)
		{
			_repositoryUser = repositoryUser;
		}

		/// <summary>
		/// Get a user by account
		/// </summary>
		/// <param name="account"></param>
		/// <returns></returns>
		public Task<User> GetUser(string account)
		{
			return _repositoryUser.Entities.SingleOrDefaultAsync(a => a.Account == account);
		}

		/// <summary>
		/// Get a user by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public Task<User> GetUser(long id)
		{
			return _repositoryUser.Entities.SingleOrDefaultAsync(a => a.Id == id);
		}

		/// <summary>
		/// Get user by id and account
		/// </summary>
		/// <param name="id"></param>
		/// <param name="account"></param>
		/// <returns></returns>
		public Task<User> CheckUser(long id, string account)
		{
			return _repositoryUser.Entities.SingleOrDefaultAsync(a => a.Id == id && a.Account == account);
		}

		/// <summary>
		/// Get users
		/// </summary>
		/// <param name="name"></param>
		/// <param name="pageIndex"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public Task<UserPageOutput> GetUsers(string name, int pageIndex, int pageSize)
		{
			return Task.FromResult(new UserPageOutput
			{
				total = _repositoryUser.Entities.Where(a => string.IsNullOrEmpty(name) ? true : a.Name.Contains(name)).Count(),
				data = _repositoryUser.Entities.Where(a => string.IsNullOrEmpty(name) ? true : a.Name.Contains(name)).Skip(pageIndex * pageSize).Take(pageSize).ToList()
			});
		}

		/// <summary>
		/// Save user
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public Task<int> SaveUser(User user)
		{
			if (user.Id > 0)
            {
                _repositoryUser.Modify(user);
            }
            else
            {
                _repositoryUser.Add(user);
            }

            return _repositoryUser.SaveChangesAsync();
		}

		/// <summary>
		/// Logic delete user
		/// </summary>
		/// <param name="id"></param>
		/// <param name="operator"></param>
		/// <returns></returns>
		public async Task<int> DeleteUser(long id, User @operator)
		{
			var user = await _repositoryUser.Entities.Where(a => a.Id == id).SingleOrDefaultAsync();
			user.IsDeleted = true;
			user.DeletedAt = DateTime.Now;
			user.DeletedBy = @operator.Id;

			_repositoryUser.Modify(user);
			return await _repositoryUser.SaveChangesAsync();
		}
	}
}
