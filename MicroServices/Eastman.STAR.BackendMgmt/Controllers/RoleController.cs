﻿using Eastman.STAR.BackendMgmt.Models.Dtos;
using Eastman.STAR.BackendMgmt.Services;
using MicroService.BaseService.Controllers;
using MicroService.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt.Controllers
{
    /// <summary>
    /// RoleController
    /// </summary>
    public class RoleController : BaseAPIController
    {
        private readonly RoleServices _roleService;

        /// <summary>
        /// RoleController Constructor
        /// </summary>
        /// <param name="roleService"></param>
        public RoleController(RoleServices roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Get Roles
        /// </summary>
        /// <param name="name"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetRoles(string name, PaginationRequest request)
        {
            try
            {
                var roles = await _roleService.GetRoles(name, request);
                return Ok(MessageResult.SuccessResult(roles, request.TotalCount));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Get Single Role By Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetRole(long roleId)
        {
            try
            {
                var role = await _roleService.GetRole(roleId);
                return Ok(MessageResult.SuccessResult(role));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddRole(RoleInput request)
        {
            try
            {
                var check = await _roleService.CheckRole(request);
                if (!check.Status)
                {
                    return Ok(MessageResult.FailureResult(check.Message));
                }

                var result = await _roleService.AddRole(request, CurrentUser.Id);
                return result.Status ? Ok(MessageResult.SuccessResult()) :
                                       Ok(MessageResult.FailureResult(result.Message));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateRole(RoleInput request)
        {
            try
            {
                var check = await _roleService.CheckRole(request);
                if (!check.Status)
                {
                    return Ok(MessageResult.FailureResult(check.Message));
                }

                var result = await _roleService.UpdateRole(request, CurrentUser.Id);
                return result.Status ? Ok(MessageResult.SuccessResult()) : 
                                       Ok(MessageResult.FailureResult(result.Message));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteRole(long roleId)
        {
            try
            {
                var result = await _roleService.DeleteRole(roleId, CurrentUser.Id);
                return result.Status ? Ok(MessageResult.SuccessResult()) :
                                       Ok(MessageResult.FailureResult());
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }
    }
}
