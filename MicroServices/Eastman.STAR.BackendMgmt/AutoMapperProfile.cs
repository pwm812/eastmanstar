﻿using AutoMapper;

using Eastman.STAR.BackendMgmt.Models.Dtos;

using MicroService.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt
{
	/// <summary>
	/// AutoMapper settings class
	/// </summary>
	public class AutoMapperProfile : Profile
	{
        /// <summary>
        /// AutoMapper Profile
        /// </summary>
		public AutoMapperProfile()
		{
			CreateMap<User, UserInput>().ReverseMap();
			CreateMap<User, UserOutput>().ReverseMap();
			CreateMap<Role, RoleInput>().ReverseMap();
        }
	}
}
