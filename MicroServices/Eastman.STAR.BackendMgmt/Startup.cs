using AutoMapper;

using Eastman.STAR.BackendMgmt.Services;

using MicroService.BaseService;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eastman.STAR.BackendMgmt
{
	public class Startup : BaseStartup
	{
		IConfiguration _configuration;

		public Startup(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public override void AfterConfigure(IApplicationBuilder app, IWebHostEnvironment env)
		{

		}

		public override void AfterConfigureServices(IServiceCollection services)
		{
			//add bearer auth
			services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
			{
				var section = _configuration.GetSection("IdentityServerSettings");
				options.Authority = section["Authority"];//IdentityServer address
				options.RequireHttpsMetadata = false;
				options.Audience = section["Audience"];
			});

			//inject business object
			services.AddTransient<UserServices>();
			services.AddTransient<RoleServices>();

            //add automapper
            services.AddAutoMapper(typeof(AutoMapperProfile));
        }
	}
}
