﻿using MicroService.Models.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroService.Framework.Gateway
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //consul settings
            var section = Configuration.GetSection("ConsulRegisterSettings");
            services.Configure<ConsulRegisterSettings>(section);
            var consulRegisterSettings = new ConsulRegisterSettings();
            section.Bind(consulRegisterSettings);

            services.AddOcelot(Configuration).AddConsul();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("ApiGateway", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "ApiGetway", Version = "v1" });
            });

            services.AddControllers();

            //services.AddConsul();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //app.UseHttpsRedirection();
            app.UseRouting();

            List<string> apis;
            //get registered service in consul
            using (var consulClient = new Consul.ConsulClient(options =>
            {
                string test = Configuration["ConsulRegisterSettings:Address"];
                options.Address = new Uri(Configuration["ConsulRegisterSettings:Address"]);
            }))
            {
                var services = consulClient.Agent.Services().Result;
                apis = services.Response.Select(a => a.Value.Service).Distinct().ToList();
            }

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                apis.ForEach(m =>
                {
                    options.SwaggerEndpoint($"/{m}/{m}/swagger.json", m);
                });
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllers();
            });

            app.UseOcelot().Wait();
        }
    }
}
