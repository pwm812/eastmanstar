﻿using MicroService.BaseService.Context.Configurations;
using MicroService.Models;
using MicroService.Models.Entities.BaseManage;
using MicroService.Utils;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.BaseService
{
	/// <summary>
	/// DbContext
	/// </summary>
	public class STARContext : DbContext
	{
		public STARContext(DbContextOptions<STARContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//initialize datas and entity
			EntityCfg.EntitySettings(modelBuilder);
			EntityCfg.InitializeDatas(modelBuilder);

			base.OnModelCreating(modelBuilder);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}

		public DbSet<User> Users { get; set; }

		public DbSet<Organization> Organizations { get; set; }

		public DbSet<Role> Roles { get; set; }
	
        public DbSet<Permission> Permissions { get; set; }
    }
}
