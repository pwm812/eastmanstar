﻿using MicroService.Models;
using MicroService.Models.Entities.BaseManage;
using MicroService.Utils;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.BaseService.Context.Configurations
{
	public class EntityCfg
	{
		public static void EntitySettings(ModelBuilder modelBuilder)
		{
			//set filter
			modelBuilder.Entity<User>().HasQueryFilter(a => !a.IsDeleted);
			modelBuilder.Entity<Organization>().HasQueryFilter(a => !a.IsDeleted);
			modelBuilder.Entity<Permission>().HasQueryFilter(a => !a.IsDeleted);
			//modelBuilder.Entity<User>().Property(a => a.Id).ValueGeneratedNever();
		}

		public static void InitializeDatas(ModelBuilder modelBuilder)
		{
			//set default data
			modelBuilder.Entity<User>().HasData(new User { Id = 1, Name = "Administrator", Account = "admin", Password = MD5Helper.Md5("123456"), CreatedAt = DateTime.Now, CreatedBy = 1 });
		}
	}
}
