﻿
using MicroService.BaseService.Middlewares;
using MicroService.Models.Models;
using MicroService.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;

namespace MicroService.BaseService
{
    /// <summary>
    /// The startup class
    /// </summary>
    public class BaseStartup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //add config
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile("config/consulSettings.json")
                .AddJsonFile("config/dbSettings.json")
                .AddEnvironmentVariables()
                .Build();

            //consul settings
            var section = configuration.GetSection("ConsulRegisterSettings");
            services.Configure<ConsulRegisterSettings>(section);
            var consulRegisterSettings = new ConsulRegisterSettings();
            section.Bind(consulRegisterSettings);

            //set allow cross-domain
            services.AddCors(options =>
            {
                options.AddPolicy("Cors", policy =>
                {
                    policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();//todo:only allowed eastman.com
                });
            });

            //services.AddHsts(options => { options.IncludeSubDomains = true; });
            //services.AddHttpsRedirection(options =>
            //{
            //    options.RedirectStatusCode = StatusCodes.Status301MovedPermanently;
            //    options.HttpsPort = consulRegisterSettings.ServiceSettings.Port;
            //});

            //add swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(consulRegisterSettings.ServiceSettings.Name, new Microsoft.OpenApi.Models.OpenApiInfo { Title = consulRegisterSettings.ServiceSettings.DisplayName, Version = "v1" });
                //add comment to swagger doc
                options.IncludeXmlComments(() =>
                {
                    var doc = new XmlDocument();
                    var allDoc = new XmlDocument();
                    allDoc.AppendChild(allDoc.CreateElement("doc"));

                    var element = allDoc.CreateElement("members");
                    var allDocPaths = Directory.GetFiles(AppContext.BaseDirectory, "*.xml");
                    foreach (var item in allDocPaths)
                    {
                        doc.Load(item);
                        var node = doc.DocumentElement.ChildNodes.Cast<XmlNode>().SingleOrDefault(a => a.LocalName == "members");
                        if (node != null)
                        {
                            foreach (var n in node.ChildNodes.Cast<XmlNode>())
                            {
                                element.AppendChild(allDoc.ImportNode(n, true));
                            }
                        }
                    }
                    allDoc.DocumentElement.AppendChild(element);
                    return new XPathDocument(new StringReader(allDoc.OuterXml));
                }, true);

            });

            //use consul custom extension method
            services.AddConsul();

            //add database
            services.AddDbContext<STARContext>(cfg =>
            {
                //cfg.UseSqlServer(configuration.GetConnectionString("SqlServer"));
                cfg.UseMySQL(configuration.GetConnectionString("MySql"));
            });

            //add redis
            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = configuration.GetConnectionString("Redis");
            });

			services.AddTransient<DbContext, STARContext>();
			services.AddDatabase();

            AfterConfigureServices(services);

            services.AddControllers();
        }

        /// <summary>
        /// do some action, like injection etc.
        /// </summary>
        /// <param name="services"></param>
        public virtual void AfterConfigureServices(IServiceCollection services) { }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHsts();
            //for https protocol
            //app.UseHttpsRedirection();
            //allow cross-domain
            app.UseCors("Cors");

            app.UseSwagger(options => { options.RouteTemplate = "{documentName}/swagger.json"; });
            app.UseSwaggerUI(options =>
            {
                var consulRegisterSettings = app.ApplicationServices.GetService<IOptions<ConsulRegisterSettings>>().Value;
                options.SwaggerEndpoint($"/{consulRegisterSettings.ServiceSettings.Name}/swagger.json", consulRegisterSettings.ServiceSettings.Name);
            });

            //Use Decrypt Middleware
            app.UseMiddleware<DecryptMiddleware>();

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            AfterConfigure(app, env);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// do some action, like use some middleware etc.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public virtual void AfterConfigure(IApplicationBuilder app, IWebHostEnvironment env) { }
    }
}
