﻿
using MicroService.Models;
using MicroService.Models.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;
using System.Linq;

namespace MicroService.BaseService.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseAPIController : ControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Health()
        {
            return Ok("alive");
        }

        User _currentUser;

        public User CurrentUser
        {
            get
            {
                if (_currentUser != null)
                {
                    return _currentUser;
                }

                var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == ClaimDefined.Account);
                //Get user from cache.
                var json = HttpContext.RequestServices.GetService<IDistributedCache>().GetString(claim.Value);
                //If cache has no user, get from db.
                if (string.IsNullOrEmpty(json))
                {
                    var repository = HttpContext.RequestServices.GetService<IRepository<User>>();
                    _currentUser = repository.Entities.FirstOrDefault(a => a.Account == claim.Value);
                }
                else
                {
                    _currentUser = JsonConvert.DeserializeObject<User>(json);
                }

                return _currentUser;
            }
            set
            {
                _currentUser = value;
                HttpContext.RequestServices.GetService<IDistributedCache>().SetString(_currentUser.Account, JsonConvert.SerializeObject(_currentUser));
            }
        }
    }
}
