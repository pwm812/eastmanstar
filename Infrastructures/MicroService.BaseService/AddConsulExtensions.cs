﻿using Consul;
using Grpc.Core;
using Grpc.Health.V1;
using Grpc.HealthCheck;
using MicroService.Models.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

using System;
using System.Threading.Tasks;

namespace MicroService.BaseService
{
    public static class AddConsulExtensions
    {
        //Add consul extenstion method
        public static void AddConsul(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var consulRegisterSettings = serviceProvider.GetService<IOptions<ConsulRegisterSettings>>().Value;
            var client = new ConsulClient(consulClientCfg =>
            {
                consulClientCfg.Address = new Uri(consulRegisterSettings.Address);
            });
            if (consulRegisterSettings.ServiceSettings == null)
            {
                return;
            }

            string serverId = consulRegisterSettings.ServiceSettings.Name + Guid.NewGuid();
            var result = client.Agent.ServiceRegister(new AgentServiceRegistration()
            {
                ID = serverId,
                Name = consulRegisterSettings.ServiceSettings.Name,
                Address = consulRegisterSettings.ServiceSettings.IP,
                Port = consulRegisterSettings.ServiceSettings.Port ?? 80,
                Check = new AgentServiceCheck //Health check
                {
                    DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5),//Deregister
                    Interval = TimeSpan.FromSeconds(10),
                    HTTP = $"{consulRegisterSettings.ServiceSettings.Scheme}://{consulRegisterSettings.ServiceSettings.IP}:{consulRegisterSettings.ServiceSettings.Port}/api/BaseAPI/Health",//Health check address.
                    Timeout = TimeSpan.FromSeconds(5)
                }
            });


            //Service Stop callback
            var lifetime = serviceProvider.GetService<IHostApplicationLifetime>();
            lifetime.ApplicationStopping.Register(() =>
            {
                Console.WriteLine("Deregister");
                client.Agent.ServiceDeregister(serverId).Wait();
            });
        }

        //public static void AddConsul(this IApplicationBuilder builder)
        //{
        //    var serviceProvider = builder.ApplicationServices;
        //    var consulRegisterSettings = serviceProvider.GetService<IOptions<ConsulRegisterSettings>>().Value;
        //    var client = new ConsulClient(consulClientCfg =>
        //    {
        //        consulClientCfg.Address = new Uri(consulRegisterSettings.Address);
        //    });

        //    var address = builder.ServerFeatures.Get<IServerAddressesFeature>().Addresses.FirstOrDefault();
        //    //var serviceProvider = services.BuildServiceProvider();
        //    //var consulRegisterSettings = serviceProvider.GetService<IOptions<ConsulRegisterSettings>>().Value;
        //    //var client = new ConsulClient(consulClientCfg =>
        //    //{
        //    //    consulClientCfg.Address = new Uri(consulRegisterSettings.Address);
        //    //});
        //    if (consulRegisterSettings.ServiceSettings == null)
        //        return;

        //    builder.ApplicationServices.GetService<ILogger<BaseStartup>>().LogInformation(address);

        //    string serverId = consulRegisterSettings.ServiceSettings.Name + Guid.NewGuid();
        //    var result = client.Agent.ServiceRegister(new AgentServiceRegistration()
        //    {
        //        ID = serverId,
        //        Name = consulRegisterSettings.ServiceSettings.Name,
        //        Address = new Uri(address).Host,
        //        Port = new Uri(address).Port,
        //        Check = new AgentServiceCheck //Health check
        //        {
        //            DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5),//Deregister
        //            Interval = TimeSpan.FromSeconds(10),
        //            HTTP = $"{address}/api/BaseAPI/Health",//Health check address.
        //            Timeout = TimeSpan.FromSeconds(5)
        //        }
        //    });


        //    //Service Stop callback
        //    var lifetime = serviceProvider.GetService<IHostApplicationLifetime>();
        //    lifetime.ApplicationStopping.Register(() =>
        //    {
        //        Console.WriteLine("Deregister");
        //        client.Agent.ServiceDeregister(serverId).Wait();
        //    });
        //}
    }

    public class HealthCheckService : HealthServiceImpl
    {
        public override Task<HealthCheckResponse> Check(HealthCheckRequest request, ServerCallContext context)
        {
            //TODO:检查逻辑
            return Task.FromResult(new HealthCheckResponse() { Status = HealthCheckResponse.Types.ServingStatus.Serving });
        }

        public override async Task Watch(HealthCheckRequest request, IServerStreamWriter<HealthCheckResponse> responseStream, ServerCallContext context)
        {
            //TODO:检查逻辑
            await responseStream.WriteAsync(new HealthCheckResponse()
            { Status = HealthCheckResponse.Types.ServingStatus.Serving });
        }
    }
}
