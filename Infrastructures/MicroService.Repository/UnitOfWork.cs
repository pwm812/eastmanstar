﻿using MicroService.Models;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Repository
{
	public class UnitOfWork : IUnitOfWork
	{
		DbContext _dbContext;
		public UnitOfWork(DbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public IRepository<T> GetStore<T>() where T : class
		{
			return new Repository<T>(_dbContext);
		}

		public int SaveChanges()
		{
			return _dbContext.SaveChanges();
		}

		public Task<int> SaveChangesAsync(CancellationToken token = default)
		{
			if (token == default)
            {
                token = new CancellationTokenSource(30 * 1000).Token;
            }
				

			return _dbContext.SaveChangesAsync(token);
		}

		public void Dispose()
		{
			_dbContext?.Database?.CloseConnection();
			_dbContext?.Dispose();
		}
	}
}
