﻿using MicroService.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Repository
{
	public static class DatabaseExtension
	{
		/// <summary>
		/// add db and inject repository and unitofwork
		/// </summary>
		/// <param name="services"></param>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static IServiceCollection AddDatabase(this IServiceCollection services)
		{
			services.AddTransient<IUnitOfWork, UnitOfWork>();
			services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

			return services;
		}
	}
}
