﻿namespace MicroService.Models.Models
{
    public class ConsulRegisterSettings
    {
        public string Address { get; set; }

        public MicroServiceSettings ServiceSettings { get; set; }
    }

    public class MicroServiceSettings
    {
        public string Scheme { get; set; }

        public string IP { get; set; }

        public int? Port { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }
    }
}
