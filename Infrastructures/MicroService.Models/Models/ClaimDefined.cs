﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Models.Models
{
	/// <summary>
	/// Defined some user claims
	/// </summary>
	public class ClaimDefined
	{
		/// <summary>
		/// User login name
		/// </summary>
		public const string Account = "Account";
	}
}
