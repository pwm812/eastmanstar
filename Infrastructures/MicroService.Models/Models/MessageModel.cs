﻿using MicroService.Models.Enums;

namespace MicroService.Models
{
    /// <summary>
    /// return message code to client
    /// </summary>
    public struct MessageModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageModel" /> struct.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="message">The message.</param>
        /// <param name="errorType">Type of the error.</param>
        public MessageModel(int code, string message, ErrorType errorType = ErrorType.General)
        {
            Code = int.Parse($"{(int)errorType}{code.ToString().PadLeft(4, '0')}");
            Message = message;
        }


        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets the general successful message.
        /// </summary>
        public static MessageModel GeneralSuccessfulMessage => new MessageModel(0, "Operate successfully");

        /// <summary>
        /// Gets the general failed message.
        /// </summary>
        public static MessageModel GeneralFailedMessage => new MessageModel(999, "Operate failed");

        /// <summary>
        /// Gets the required account and password message.
        /// </summary>
        public static MessageModel RequiredAccountAndPasswordMessage => new MessageModel(1, "Account and password are required", ErrorType.Account);

        /// <summary>
        /// Gets the wrong account or password message.
        /// </summary>
        public static MessageModel WrongAccountOrPasswordMessage => new MessageModel(2, "Account or password is wrong", ErrorType.Account);

        /// <summary>
        /// Gets the authentication failed message.
        /// </summary>
        public static MessageModel AuthenticationFailedMessage => new MessageModel(3, "Authentication failed", ErrorType.Account);

        /// <summary>
        /// Gets the no authentication message.
        /// </summary>
        public static MessageModel NoAuthenticationMessage => new MessageModel(4, "No authentication", ErrorType.Account);

        /// <summary>
        /// Gets the identifier or account changed message.
        /// </summary>
        public static MessageModel IdOrAccountChangedMessage => new MessageModel(5, "Id Or Account Is Changed", ErrorType.Account);

        /// <summary>
        /// Gets the account already existed message.
        /// </summary>
        public static MessageModel AccountAlreadyExistedMessage => new MessageModel(6, "Account Already Existed", ErrorType.Account);

        /// <summary>
        /// Gets the invalid identifier message.
        /// </summary>
        public static MessageModel InvalidIdMessage => new MessageModel(7, "Id Is Invalid", ErrorType.Account);

        /// <summary>
        /// Gets the add or update to be an existed role name.
        /// </summary>
        public static MessageModel AddOrUpdateToBeAnExistedRoleName => new MessageModel(8, "Add Or Update To Be An Existed Role Name", ErrorType.Account);

        /// <summary>
        /// Gets the role name is empty message.
        /// </summary>
        public static MessageModel RoleNameIsEmptyMessage => new MessageModel(9, "Role name is empty", ErrorType.Account);
    }
}
