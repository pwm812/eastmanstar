﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Models
{
	public interface IUnitOfWork : IDisposable
	{
		/// <summary>
		/// do save async
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		Task<int> SaveChangesAsync(CancellationToken token = default(CancellationToken));


		/// <summary>
		/// do save
		/// </summary>
		/// <returns></returns>
		int SaveChanges();

		IRepository<T> GetStore<T>() where T : class;
	}
}
