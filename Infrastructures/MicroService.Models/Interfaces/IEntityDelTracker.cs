﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Models
{
	public interface IEntityDelTracker
	{
		public bool IsDeleted { get; set; }

		public DateTime? DeletedAt { get; set; }

		public long? DeletedBy { get; set; }
	}
}
