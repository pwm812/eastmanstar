﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Models
{
	public interface IEntityTracker
	{
		public DateTime CreatedAt { get; set; }

		public long CreatedBy { get; set; }

		public DateTime? ModifiedAt { get; set; }

		public long? ModifiedBy { get; set; }
	}
}
