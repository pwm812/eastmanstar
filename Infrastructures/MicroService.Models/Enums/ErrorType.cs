﻿namespace MicroService.Models.Enums
{
    /// <summary>
    /// Defines special type for custom errors
    /// </summary>
    public enum ErrorType
    {
        /// <summary>
        /// The general
        /// </summary>
        General = 9,

        /// <summary>
        /// The account
        /// </summary>
        Account = 1,

        /// <summary>
        /// The unknow
        /// </summary>
        Unknown = 0
    }
}
