﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Models.Entities.BaseManage
{
    public class Permission : BaseEntity<long>
    {
        /// <summary>
        /// ShortName
        /// </summary>
        [MaxLength(100)]
        public string ShortName { get; set; }

        /// <summary>
        /// FullName
        /// </summary>
        [MaxLength(200)]
        public string FullName { get; set; }

        /// <summary>
        /// Type :  menu  button
        /// </summary>
        [MaxLength(50)]
        public string Type { get; set; }

        [MaxLength(20)]
        public string Code { get; set; }

        [MaxLength(250)]
        public string ParentId { get; set; }

        [MaxLength(20)]
        public string Enable { get; set; }
    }
}
