﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Models.Entities.BaseManage
{
	public class Organization : BaseEntity<long>
	{
		[MaxLength(100)]
		public string ShortName { get; set; }

		[MaxLength(200)]
		public string FullName { get; set; }

		[MaxLength(50)]
		public string LegalPerson { get; set; }

		[MaxLength(20)]
		public string Telephone { get; set; }

		[MaxLength(250)]
		public string Address { get; set; }

		[MaxLength(20)]
		public string Longitude { get; set; }

		[MaxLength(20)]
		public string Latitude { get; set; }
	}
}
