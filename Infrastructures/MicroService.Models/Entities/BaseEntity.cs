﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Models
{
	public class BaseEntity<T> : IEntityTracker, IEntityDelTracker
	{
		public T Id { get; set; }
		public DateTime CreatedAt { get; set; }
		public long CreatedBy { get; set; }
		public DateTime? ModifiedAt { get; set; }
		public long? ModifiedBy { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime? DeletedAt { get; set; }
		public long? DeletedBy { get; set; }
	}
}
