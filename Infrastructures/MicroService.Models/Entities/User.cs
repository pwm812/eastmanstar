﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Models
{
	public class User : BaseEntity<long>
	{
		/// <summary>
		/// Organization's pk
		/// </summary>
		public long OrganizationId { get; set; }

		[MaxLength(255)]
		public string Name { get; set; }

		[MaxLength(32)]
		public string Account { get; set; }

		[MaxLength(50)]
		public string Password { get; set; }

		[MaxLength(100)]
		public string Email { get; set; }

		[MaxLength(20)]
		public string Phone { get; set; }
	}
}
