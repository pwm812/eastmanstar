﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Models
{
    /// <summary>
    /// Role Entity
    /// </summary>
    public class Role : BaseEntity<long>
    {
        /// <summary>
        /// Role Name
        /// </summary>
        [StringLength(20)]
        public string RoleName { get; set; }

        /// <summary>
        /// Parent Role Id
        /// Dealing with hierarchical relationships between roles
        /// </summary>
        public long? ParentRoleId { get; set; }

        /// <summary>
        /// Description About Role
        /// </summary>
        [StringLength(100)]
        public string Description { get; set; }
    }
}
