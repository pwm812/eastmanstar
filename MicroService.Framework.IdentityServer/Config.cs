﻿using IdentityServer4;
using IdentityServer4.Models;

using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer
{
	public class Config
	{
		/// <summary>
		/// Get api resource from appsettings.json
		/// </summary>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static IEnumerable<ApiResource> GetApiResources(IConfiguration configuration)
		{
			var resources = new List<ApiResource>();
			configuration.GetSection("ApiResources").Bind(resources);
			return resources;
		}

		/// <summary>
		/// Get api scope from appsettings.json
		/// </summary>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static IEnumerable<ApiScope> GetApiScopes(IConfiguration configuration)
		{
			var scopes = new List<ApiScope>();
			configuration.GetSection("ApiScopes").Bind(scopes);
			return scopes;
		}

		/// <summary>
		/// Get client from appsettings.json
		/// </summary>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static IEnumerable<Client> GetClients(IConfiguration configuration)
		{
			var clients = new List<Client>();
			configuration.GetSection("Clients").Bind(clients);

			foreach (var client in clients)
			{
				foreach (var item in client.ClientSecrets)
				{
					item.Value = item.Value.Sha256();
				}
			}

			return clients;
		}
	}
}
