﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Models
{
	public class AuthModel
	{
		public Authorization BackendAuth { get; set; }

		public Authorization MiniProgramAuth { get; set; }
	}

	public class Authorization
	{
		public string ClientId { get; set; }

		public string ClientSecret { get; set; }

		public string Scope { get; set; }
	}
}
