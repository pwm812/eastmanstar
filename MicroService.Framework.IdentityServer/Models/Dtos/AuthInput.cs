﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Models.Dtos
{
    public class LogoutInput
    {
        public string Token { get; set; }
    }
}
