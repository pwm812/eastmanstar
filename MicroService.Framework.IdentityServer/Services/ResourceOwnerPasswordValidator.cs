﻿using IdentityModel;

using IdentityServer4.Models;
using IdentityServer4.Validation;

using MicroService.Framework.IdentityServer.Services;
using MicroService.Models;
using MicroService.Models.Models;

using Microsoft.Extensions.Caching.Distributed;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        AuthService _authService;
        IDistributedCache _distributedCache;

        public ResourceOwnerPasswordValidator(AuthService authService, IDistributedCache distributedCache)
        {
            _authService = authService;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// check the user is validation, add uaername into claims
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            User user;
            var json = await _distributedCache.GetStringAsync(context.UserName);
            if (!string.IsNullOrEmpty(json))
            {
                user = JsonConvert.DeserializeObject<User>(json);
            }
            else
            {
                user = await _authService.GetUser(context.UserName, context.Password);
                //set user entity to cache
                _distributedCache.SetString(user.Account, JsonConvert.SerializeObject(user));
            }

            if (user != null)
            {
                context.Result = new GrantValidationResult(
                    subject: context.UserName,
                    authenticationMethod: OidcConstants.AuthenticationMethods.Password,
                    claims: new List<Claim>
                    {
                        new Claim(ClaimDefined.Account, context.UserName),
                    });
            }
            else
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "invalid");
            }
        }
    }
}
