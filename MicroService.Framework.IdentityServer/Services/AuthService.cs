﻿using IdentityModel.Client;

using MicroService.Framework.IdentityServer.Models;
using MicroService.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer.Services
{
    public class AuthService
    {
        AuthModel _authModel;
        IRepository<User> _repositoryUser;
        IHttpContextAccessor _httpContextAccessor;

        public AuthService(IOptions<AuthModel> options, IRepository<User> repositoryUser, IHttpContextAccessor httpContextAccessor)
        {
            _authModel = options.Value;
            _repositoryUser = repositoryUser;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Generate token
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public async Task<string> BackendAuth(string account, string pwd)
        {
            var httpClient = new HttpClient();
            var disco = await httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}",
                Policy =
                {
                     RequireHttps = false,
                     ValidateEndpoints = false,
                     ValidateIssuerName = false
                }
            });
            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }
            var tokenResponse = await httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                UserName = account,
                Password = pwd,
                ClientId = _authModel.BackendAuth.ClientId,
                ClientSecret = _authModel.BackendAuth.ClientSecret,
                Scope = _authModel.BackendAuth.Scope
            });
            return tokenResponse.AccessToken;
        }

        /// <summary>
        /// Generate token
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public async Task<TokenRevocationResponse> Logout(string token)
        {
            var httpClient = new HttpClient(); ;
            var disco = httpClient.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.Value}",
                Policy =
                {
                     RequireHttps = false,
                     ValidateEndpoints = false,
                     ValidateIssuerName = false
                }
            }).Result;

            var result = await httpClient.RevokeTokenAsync(new TokenRevocationRequest
            {
                Address = disco.RevocationEndpoint,
                ClientId = _authModel.BackendAuth.ClientId,
                ClientSecret = _authModel.BackendAuth.ClientSecret,
                Token = token
            });

            return result;
        }

        public string MiniProgramAuth()
		{
			return string.Empty;
		}

        /// <summary>
        /// Get user by account and pwd.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public Task<User> GetUser(string account, string pwd)
        {
            return _repositoryUser.Entities.FirstOrDefaultAsync(a => a.Account == account && a.Password == pwd);
        }
    }
}
