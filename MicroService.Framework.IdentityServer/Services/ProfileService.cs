﻿using IdentityServer4.Models;
using IdentityServer4.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Framework.IdentityServer
{
	public class ProfileService : IProfileService
	{
		/// <summary>
		/// custom token claims
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public Task GetProfileDataAsync(ProfileDataRequestContext context)
		{
			try
			{
				//set user token claims
				context.IssuedClaims = context.Subject.Claims.ToList();
			}
			catch (Exception ex)
			{
				//log your error
			}
			return Task.CompletedTask;
		}

		public Task IsActiveAsync(IsActiveContext context)
		{
			context.IsActive = true;
			return Task.CompletedTask;
		}
	}
}
