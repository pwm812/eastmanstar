﻿using MicroService.BaseService.Controllers;
using MicroService.Framework.IdentityServer.Models;
using MicroService.Framework.IdentityServer.Models.Dtos;
using MicroService.Framework.IdentityServer.Services;
using MicroService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace MicroService.Framework.IdentityServer.Controllers
{
    public class AuthController : BaseAPIController
    {
        AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Use account and pwd get token
        /// </summary>
        /// <param name="userLoginModel">The user login model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult BackendAuth([FromBody] UserLoginModel userLoginModel)
        {
            try
            {
                var token = _authService.BackendAuth(userLoginModel.Account, userLoginModel.Password).Result;
                return string.IsNullOrEmpty(token)
                    ? Ok(MessageResult.FailureResult(MessageModel.AuthenticationFailedMessage))
                    : Ok(MessageResult.SuccessResult(token));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        /// <summary>
        /// Use account and pwd get token
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Logout([FromBody] LogoutInput input)
        {
            try
            {
                var res = _authService.Logout(input.Token).Result;
                return Ok(MessageResult.SuccessResult(res));
            }
            catch (Exception ex)
            {
                return Ok(MessageResult.FailureResult(new MessageModel(9999, ex.Message)));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult MiniProgramAuth()
        {
            throw new NotImplementedException();
        }
    }
}
