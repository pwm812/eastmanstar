#!/bin/bash

cd /src/MicroService.BaseService

until dotnet ef database update -v --no-build; do
>&2 echo "SQL Server is starting up"
sleep 1
done
